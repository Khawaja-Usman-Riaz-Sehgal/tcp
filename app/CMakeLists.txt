cmake_minimum_required(VERSION 3.16.3)
project(TCP)


include_directories(${CMAKE_SOURCE_DIR}/include)

file(GLOB server_src ${CMAKE_SOURCE_DIR}/src/TcpServer.cpp)
file(GLOB client_src ${CMAKE_SOURCE_DIR}/src/TcpClient.cpp)

file(GLOB client_app ${CMAKE_SOURCE_DIR}/app/main.cpp)
file(GLOB server_app ${CMAKE_SOURCE_DIR}/app/main-server.cpp)

file(GLOB epoll_src ${CMAKE_SOURCE_DIR}/src/epoll.cpp)
file(GLOB epoll_app ${CMAKE_SOURCE_DIR}/app/epoll-main.cpp)

add_executable(out ${client_src} ${client_app})
add_executable(out-server ${server_src} ${server_app})
add_executable(epoll-main ${epoll_src} ${epoll_app})

target_link_libraries(out-server out1)
target_link_libraries(out out1)
target_link_libraries(epoll-main out1)
