#include <iostream>
#include "../include/TcpClient.h"
using namespace std;
int main(int argc, char **argv)
{
   if (argc != 3)
   {
      cout << "Please Enter correct number of arguments \n";
      cout << "Usage : ./app/out-server ip port" << endl;
      return 0;
   }
   TcpClient tcp;
   string ip = argv[1];
   int port = atoi(argv[2]);
   tcp.set_tcp_ip_and_port(ip, port);
   try
   {
      tcp.connect();
      tcp.continuous_receive();
   }
   catch (char const *message)
   {
      cout << "EXCEPTION:" << message << endl;
   }
   catch (int error)
   {
      cout << "Errno: \n"
           << strerror(error) << endl;
   }
return 0;
}