#include <iostream>
#include "../include/TcpServer.h"
using namespace std;
int main(int argc, char **argv)
{
   if (argc != 3)
   {
      cout << "Please Enter correct number of arguments \n";
      cout << "Usage : ./app/out-server ip port" << endl;
      return 0;
   }

   TcpServer tcpserver;
   string ip = argv[1];
   int port = atoi(argv[2]);

   try
   {
      tcpserver.socket_creation(ip, port);
      tcpserver.bind();
      tcpserver.listen();
      tcpserver.accept();
      tcpserver.continuous_receive();
   }
   catch (char const *message)
   {
      cout << "EXCEPTION:" << message << endl;
   }
   catch (int error)
   {
      cout << "Errno: \n"
           << strerror(error) << endl;
   }
return 0;
}