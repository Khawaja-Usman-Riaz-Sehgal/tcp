#include "../include/epoll.h"
#include "errno.h"
epoll::epoll()
{

    epoll_fd = epoll_create1(0);

    if (epoll_fd == -1)
    {
        throw errno;
    }
};

int epoll::add_epoll(int fd)
{

    event.events = EPOLLIN;
    event.data.fd = fd;

    if (::epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &event))
    {

        close(epoll_fd);
        throw errno;

        return 1;
    }
    return 0;
};
