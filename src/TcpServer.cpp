#include "TcpServer.h"
#include <iostream>
using namespace std;
void TcpServer::socket_creation(string ip, int port)
{
    this->port = port;
    this->ip = ip;
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        //perror("socket failed");
        throw(strerror(errno));
        //return -1;
    }
    // Forcefully attaching socket to the port 8080
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
                   &opt, sizeof(opt)))
    {
        // perror("setsockopt");
        throw(strerror(errno));
        //return -1;
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);
    // cout<<server_fd<<endl;
    // cout<<&address<<endl;
    // cout<<&addrlen<<endl;
    //return 0;
}

bool TcpServer::bind()
{
    if (::bind(server_fd, (struct sockaddr *)&address,
               sizeof(address)) < 0)
    {
        //perror("bind failed");
        throw(strerror(errno));
        //exit(EXIT_FAILURE);
        return 1;
    }
    return 0;
}
bool TcpServer::listen()
{
    if (::listen(server_fd, 3) < 0)
    {
        //perror("listen");
        //exit(EXIT_FAILURE);
        throw(strerror(errno));
        return 1;
    }
    return 0;
}
bool TcpServer::accept()
{
    if ((new_socket = ::accept(server_fd, (struct sockaddr *)&address,
                               (socklen_t *)&addrlen)) < 0)
    {
        //perror("accept");
        //exit(EXIT_FAILURE)
        throw(strerror(errno));
        return 1;
    }
    return 0;
}
bool TcpServer::continuous_receive()
{
    send(new_socket, "buffer", strlen("buffer"), 0);
    char buffer[1024] = {0};
    while (1)
    {
        valread = read(new_socket, buffer, 1024);
        cout << "Data: " << buffer << endl;
        if (valread > 0)
        {
            send(new_socket, buffer, 1024, 0);
            memset(buffer, 0, 1024);
        }
        else if (valread == 0)
        {
            return -1;
        }
    }
    return 0;
}