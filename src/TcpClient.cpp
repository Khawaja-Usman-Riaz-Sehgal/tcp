#include "TcpClient.h"
using namespace std;
void TcpClient::set_tcp_ip_and_port(string ip, int port)
{
    //struct sockaddr_in serv_addr;
    // char *hello = "Hello from client";
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        throw(strerror(errno));
    }
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if (inet_pton(AF_INET, ip.c_str(), &serv_addr.sin_addr) < 0)
    {
        throw(strerror(errno));
    }
    else if (inet_pton(AF_INET, ip.c_str(), &serv_addr.sin_addr) == 0)
    {
        throw "Invlaid ip address or port number";
    }
}
bool TcpClient::connect()
{
    if (::connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        throw(strerror(errno));
    }
    return 0;
}

bool TcpClient::send(char *input)
{

    if (::send(sock, input, strlen(input), 0) < 0)
    {
        // throw ("\nNot Data Send\n");
        throw(strerror(errno));
    }
    cout << "Data send : " << input << endl;
    return 0;
}
void TcpClient::continuous_receive()
{
    char buffer[1024] = {0};
    while (1)
    {
        valread = read(sock, buffer, 1024);
        if (valread > 0)
        {
            send(buffer);
            memset(buffer, 0, 1024);
        }
        else
        {
            cout << "Server closed!" << endl;
            break;
        }
    }
}
int TcpClient :: get_fd()
{
    return sock;
}