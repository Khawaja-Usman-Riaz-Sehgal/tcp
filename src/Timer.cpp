#include <iostream>
#include "Timer.h"
#include <errno.h>
using namespace std;
int Timer::get_fd()
{
    return timer_fd;
}
int Timer::create_timer()
{
    timer_fd = timerfd_create(CLOCK_REALTIME, 0);
    if (timer_fd < 0)
    {
        cout << "1";
        throw errno;
    }
    return timer_fd;
}
int Timer::set_timer(int initial_value, int interval)
{
    if (clock_gettime(CLOCK_REALTIME, &now) == -1)
    {
        cout << "2";
        throw(errno);
    }

    /* Create a CLOCK_REALTIME absolute timer with initial
              expiration and interval as specified in command line */

    timeout.it_value.tv_sec = now.tv_sec + initial_value;
    timeout.it_value.tv_nsec = now.tv_nsec;
    timeout.it_interval.tv_sec = interval; // 10 sec interval after first expiration
    timeout.it_interval.tv_nsec = 0;

    if (timerfd_settime(timer_fd, TFD_TIMER_ABSTIME, &timeout, NULL) == -1)
    {
        cout << "3";
        throw errno;
    }
    return 0;
}