#include <sys/timerfd.h>

class Timer
{
private:
    struct itimerspec timeout;
    struct timespec now;
    int timer_fd = 0;
public:
   int get_fd();
   int create_timer();
   int set_timer(int initial_value, int interval);

};