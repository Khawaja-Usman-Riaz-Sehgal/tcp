#pragma once
#ifndef EPOLL_H
#define EPOLL_H

#define MAX_EVENTS 5
#include <stdio.h>     // for fprintf()
#include <unistd.h>    // for close(), read()
#include <sys/epoll.h> // for epoll_create1(), epoll_ctl(), struct epoll_event
#include <string.h>    // for strncmp

class epoll 
{
public:

struct epoll_event event, events[MAX_EVENTS];
int epoll_fd;
int add_epoll(int);
epoll();
};

#endif