#pragma once

#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <string>

class TcpServer
{
private:
    int port;
    std::string ip;
    int server_fd;
    int new_socket;
    int valread = 0;
    struct sockaddr_in address;
    int addrlen = sizeof(address);
    int opt = 1;

public:
    void socket_creation(std::string ip, int port);
    bool bind();
    bool listen();
    bool accept();
    bool continuous_receive();
};