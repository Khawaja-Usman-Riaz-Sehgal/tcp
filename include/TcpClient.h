#pragma once

#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string>
#include <string.h>
#define PORT 8080

class TcpClient
{
private:
    int sock = 0;
    int valread;
    struct sockaddr_in serv_addr;

public:
    void set_tcp_ip_and_port(std::string ip, int port);
    bool connect();
    bool send(char *input_data);
    void continuous_receive();
    int get_fd();
};